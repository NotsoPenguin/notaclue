package notso.mods.nac.reference

import java.util.logging.Logger

object Reference {
  final val ModId = "NotAClue"
  final val ModName = "Not A Clue"
  final val ModClass = "notso.mod.nac.NotAClue"
  final val Version = "1.0"

  final val ClientProxy = "notso.mods.nac.client.Proxy"
  final val ServerProxy = "notso.mods.nac.server.Proxy"
  final val CommonProxy = "notso.mods.nac.common.Proxy"

  val Log = Logger.getLogger("Not A Clue")
}

