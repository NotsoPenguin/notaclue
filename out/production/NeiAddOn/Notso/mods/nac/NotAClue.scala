package notso.mods.nac

import cpw.mods.fml.common.{SidedProxy, Mod}
import cpw.mods.fml.common.event.{FMLInitializationEvent, FMLPostInitializationEvent, FMLPreInitializationEvent}
import notso.mods.nac.reference.{Reference=>Ref}




@Mod(modid = Ref.ModId ,name = Ref.ModName ,version = Ref.Version ,modLanguage = "scala")
object NotAClue{

  @Mod.Instance
  var Instance = null

  @SidedProxy(clientSide = Ref.ClientProxy, serverSide = Ref.ServerProxy)
  var Proxy:notso.mods.nac.common.Proxy = null

  @Mod.EventHandler()
  def PreInit(event:FMLPreInitializationEvent) {

  }

  @Mod.EventHandler()
  def Init(event:FMLInitializationEvent) {

  }

  @Mod.EventHandler()
  def Init(event:FMLPostInitializationEvent) {

  }



}
